#### DESCRIPTION
Based on the EDA results, it has been observed that cropped masks may contain empty masks, with 1500 out of 1764 (256,256) cropped masks being empty. Therefore, it was decided to only use images with non-empty masks for training, with either a randomizer or the first 40 images being selected. This resulted in a total of 304 images (264 + 40) for (256,256) cropping.

To determine the optimal cropping size, the pixel ratio (mask pixels/ all mask pixels) was used, with the graph showing the pixel ratio for all images received during cropping.

It was found that the masks did not cover all erosion during training, which may have an impact on model predictions.

Due to the relatively small number of training images available, albumentations will be used to create artificial images to increase the number of training samples. This will be implemented in the training file.


#### RESULTS

After several attempts, it was found that the Unet model combined with EfficientNetB0 architecture yielded the best results, and thus this approach was ultimately chosen.

- The table shows the performance metrics of a model for soil erosion detection.
- The Dice coefficient for this model is 0.0799.
- The input and mask resolution for this model is 256x256 pixels.
- The model was trained for 50 epochs with 250 steps per epoch.
- The FocalLoss function was used as the loss function for the model.
- The Adam optimizer was used during training.
- The batch size for this model was 16.


## SOLUTION REPORT

The EDA analysis showed that obtaining more satellite images is necessary, as the current dataset of only 304 images is insufficient, even with the use of albumentations. The improved performance of the Unet model combined with EfficientNetB0 architecture suggests that it might be beneficial to try using Unet with EfficientNetB3. It may also be helpful to balance the dataset by clustering images according to the ratio of mask pixels to non-mask pixels. Further improvements can be made by adjusting hyperparameters such as the learning rate, batch size, steps per epoch, or loss function.

The accuracy of soil erosion detection predictions depends on various factors such as soil type, topography, and climate specifics. Up-to-date satellite imagery can provide valuable information on crops, and historical data for several years can be analyzed to generate productivity observations using cloudless imagery analytics.

In addition to soil erosion prediction, a zoning feature could be added to help farmers understand the appropriate amounts of mineral and organic fertilizers needed for different areas based on their individual requirements. By dividing fields into several zones based on vegetation indices maps, farmers can easily distribute fertilizers according to the needs of each zone, ultimately leading to better crop yields.

